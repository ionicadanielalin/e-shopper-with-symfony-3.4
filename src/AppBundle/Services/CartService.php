<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/26/2019
 * Time: 6:52 PM
 */

namespace AppBundle\Services;


use AppBundle\Entity\Cart;
use AppBundle\Entity\CartItem;
use AppBundle\Entity\OrderItem;
use AppBundle\Entity\Orders;
use AppBundle\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CartService
{
    /** @var  TokenStorage */
    private $token_storage;

    /** @var  Registry */
    private $doctrine;

    /** @var  Session */
    private $session;

    /**
     * @return TokenStorage
     */
    public function getTokenStorage()
    {
        return $this->token_storage;
    }

    /**
     * @param TokenStorage $token_storage
     * @return CartService
     */
    public function setTokenStorage($token_storage)
    {
        $this->token_storage = $token_storage;

        return $this;
    }

    public function getCart()
    {
        $sessionCart = null;
        $userCart = null;
        //cart doar in sesiune
        if ($this->getSession()->has('cart_id')){
            $sessionCart = $this->getDoctrine()->getRepository(Cart::class)->find($this->getSession()->get('cart_id'));
        }

        //cart doar in user
        if ($this->getTokenStorage()->getToken()->getUser()!='anon.'){
            $user = $this->getTokenStorage()->getToken()->getUser();
            $userCart = $this->getDoctrine()->getRepository(Cart::class)->findOneBy(['status'=>'new', 'user'=>$user]);
        }

        if ($sessionCart && is_null($userCart)){
            if ($this->getTokenStorage()->getToken()->getUser()!='anon.'){
                $user = $this->getTokenStorage()->getToken()->getUser();
                $sessionCart->setUser($user);
                $this->getDoctrine()->getManager()->persist($sessionCart);
                $this->getDoctrine()->getManager()->flush();
            }
            return $sessionCart;
        }

        if (is_null($sessionCart) && $userCart){
            return $userCart;
        }

        if ($sessionCart && $userCart){
            return $this->merge($sessionCart, $userCart);
        }

        if (is_null($sessionCart) && is_null($userCart)){
            $cart = new Cart();
            $this->getDoctrine()->getManager()->persist($cart);
            $this->getDoctrine()->getManager()->flush();
            if ($this->getTokenStorage()->getToken()->getUser()!='anon.'){
                $user = $this->getTokenStorage()->getToken()->getUser();
                $cart->setUser($user);
                $this->getDoctrine()->getManager()->persist($cart);
                $this->getDoctrine()->getManager()->flush();
            } else {
                $this->getSession()->set('cart_id', $cart->getId());
            }

            return $cart;
        }
    }

    public function merge(Cart $sessionCart, Cart $userCart){
        foreach ($sessionCart->getCartItems() as $cartItem){
            $this->add($userCart, $cartItem->getProduct(), $cartItem->getQuantity());
        }

        $this->getDoctrine()->getManager()->remove($sessionCart);
        $this->getDoctrine()->getManager()->flush();
        $this->getSession()->remove('cart_id');

        return $userCart;
    }

    public function add(Cart $cart, Product $product, $quantity=1)
    {
        $cartItem = null;
        foreach ($cart->getCartItems() as $cartI){
            if ($cartI->getProduct()->getId() == $product->getId()){
                $cartItem = $cartI;
            }
        }
        if (is_null($cartItem)){
            $cartItem = new CartItem();
            $cartItem->setCart($cart);
            $cartItem->setProduct($product);
        }
        $cartItem->setQuantity($cartItem->getQuantity() + $quantity);
        $this->getDoctrine()->getManager()->persist($cartItem);
        $this->getDoctrine()->getManager()->flush();
    }

    public function update(CartItem $cartItem, $quantity)
    {
        if ($quantity <= 0){
            $this->getDoctrine()->getManager()->remove($cartItem);
        } else {
            $cartItem->setQuantity($quantity);
            $this->getDoctrine()->getManager()->persist($cartItem);
        }
        $this->getDoctrine()->getManager()->flush();
    }

    public function clearCart(Cart $cart)
    {
        foreach ($cart->getCartItems() as $cartItem){
            $this->getDoctrine()->getManager()->remove($cartItem);
        }
        $this->getDoctrine()->getManager()->flush();
    }

    public function checkoutCart(Cart $cart)
    {
        if ($cart->getStatus()=='new'){
            $order= new Orders();
            $order->setUser($cart->getUser());
            $order->setCart($cart);
            $this->getDoctrine()->getManager()->persist($order);
            $this->getDoctrine()->getManager()->flush();

            foreach ($cart->getCartItems() as $cartItem){
                $orderItem = new OrderItem();
                $orderItem->setQuantity($cartItem->getQuantity());
                $orderItem->setProduct($cartItem->getProduct());
                $orderItem->setPrice($cartItem->getProduct()->getPrice());
                $orderItem->setCartItem($cartItem);
                $orderItem->setOrders($order);
                $this->getDoctrine()->getManager()->persist($orderItem);
                $this->getDoctrine()->getManager()->flush();
            }
            $cart->setStatus('processed');
            $this->getDoctrine()->getManager()->persist($cart);
            $this->getDoctrine()->getManager()->flush();
        }
    }


    /**
     * @return Registry
     */
    public function getDoctrine()
    {
        return $this->doctrine;
    }

    /**
     * @param Registry $doctrine
     * @return CartService
     */
    public function setDoctrine($doctrine)
    {
        $this->doctrine = $doctrine;

        return $this;
    }

    /**
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param Session $session
     * @return CartService
     */
    public function setSession($session)
    {
        $this->session = $session;
        return $this;
    }




}