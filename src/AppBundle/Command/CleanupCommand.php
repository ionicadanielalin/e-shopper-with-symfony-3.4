<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 9/26/2019
 * Time: 9:21 PM
 */

namespace AppBundle\Command;

use AppBundle\Entity\Cart;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanupCommand extends ContainerAwareCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'shop:cleanup';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cartService = $this->getContainer()->get('cart.service');

        $carts = $this->getContainer()->get('doctrine')
            ->getRepository(Cart::class)->findBy(['status'=>'new']);

        foreach ($carts as $cart){
            $cartService->clearCart($cart);
            echo "Cartul cu id: {$cart->getId()} a fost curatat.\n";
        }
    }
}